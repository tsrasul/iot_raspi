#include <string>
#include <iostream>
#include <vector>

#include <cstdlib>

#include "wiringPi.h"
#include "wiringSerial.h"
#include "softPwm.h"
// #include "HttpRequest.h"
// #include "HttpException.h"
//url: stayhomepi
using namespace std;

const int PIR = 0, lightRPIN = 3, lightGPIN = 4, lightBPIN = 5;  //pins for pir sensor and rgb light pwm
const int lightThresh = 5, pwmRange = 50;
volatile bool lightOn = false;                 // volatile to prevent compiler optimizing
volatile bool pirActive = false;
volatile bool lowLight = false;
volatile int lightR = 0, lightG = 0, lightB = 0;

string comBT = "/dev/ttyAMA0"; //bt connected to onboard serial
string comAT = "/dev/ttyACM0"; // attiny connected via usb-serial converter
string lastSerAT = "", lastSerBT = "";  //to resend last serial command
int serialBT, serialAT;        //file descriptors returned by serialOpen

void setup();
void checkPIR();
void pirISR();
void changeLightIntensity(unsigned int r, unsigned int g, unsigned int b);
void checkSerAT();
void checkSerBT();
void parseStrData(string data);

void trim(string &data)
{
	//trim any whitespaces, newlines, tabs or carriage return from source string
	int pos = data.find_first_not_of(" \r\t\n");
	if(pos < 0)
	{
		data = "";
		return;
	}
	data = data.substr(pos);
	pos = data.find_last_not_of(" \r\n\t");
	data = data.substr(0, pos);
}

char* chr(string str)
{
	//convert std::string to char* using first pointer from string
	return &str[0];
}

int main(int argc, char const *argv[])
{
	setup();
	while(true)
	{
		checkPIR();
		checkSerAT();
		delay(10);	
	}
	return 0;
}

void setup()
{
	wiringPiSetup();  //initialise the GPIO pins
	pinMode(PIR, INPUT);  // pir pin as input
	wiringPiISR(PIR, INT_EDGE_BOTH, pirISR);  //register interrupt callback in case of rising or falling pulse
	serialBT = serialOpen(chr(comBT) , 9600);  // initialise serials
	serialAT = serialOpen(chr(comAT) , 9600);
	if(serialAT < 0)
	{
		cout << "unable to open "+comAT <<endl;    
		exit(-2);
	}
	if(serialBT < 0)
	{
		cout << "unable to open "+comBT <<endl;    
		exit(-2);
	}
	softPwmCreate(lightRPIN, lightR, pwmRange);  //initialise pwms
	softPwmCreate(lightGPIN, lightG, pwmRange);
	softPwmCreate(lightBPIN, lightB, pwmRange);
}

void checkPIR()
{
	if(pirActive && !lightOn)  // if movement detected and light is off
	{
		changeLightIntensity(50,50,50);  // turn on the lights with dim value
	}
}

void pirISR()
{
	delay(1);
	int pinStat = digitalRead(PIR);
	pirActive = pinStat;
}

void changeLightIntensity(unsigned int r, unsigned int g, unsigned int b)
{
	if(r > 250){  // maxvalue for soft_pwm is 50, 50*5 (you'll see) = 250
	    r = 250;
	}
	if(r > 250){
	    r = 250;
	}
	if(b > 250){
	    b = 250;
	}
	unsigned int mr = r/5, mg = g/5, mb = b/5;  // don't need finer resolution, just high frequency to prevent flickering
	if((mr + mg + mb) < lightThresh)
	{
		//set light master relay off
		lightOn = false;
		lastSerAT = "N";
	 	serialPuts(serialAT, chr(lastSerAT));
	}
	else
	{
		//set light master relay on and set pwm
		lightOn = true;
		lastSerAT = "P";
	 	serialPuts(serialAT, chr(lastSerAT));
	 	softPwmWrite(lightRPIN, mr);
	 	softPwmWrite(lightGPIN, mg);
	 	softPwmWrite(lightBPIN, mb);
	}
}

void checkSerAT()
{
	string temp = "";
	while(serialDataAvail(serialAT))
	{
		temp += static_cast<char>(serialGetchar(serialAT));
	}
	trim(temp);
	//consult attiny code for this
	if(temp == "NACK")
	{
		serialPuts(serialAT, chr(lastSerAT));
	}
	else if(temp == "high")
	{
		lightOn = true;
	}
	else if(temp == "low")
	{
		lightOn = false;
		lightR = lightG = lightB = 0;
	}
}

void checkSerBT()
{
	string temp = "";
	while(serialDataAvail(serialBT))
	{
		temp += static_cast<char>(serialGetchar(serialBT));
	}
	parseStrData(temp);
}

void parseStrData(string data)
{
	//trim it and separate the space delimited data to a vector of string
	trim(data);
	data = data.substr(1, data.length() - 2);
	vector<string> strArr;
	int pos = data.find(',');
	while(pos > 0)
	{
		strArr.push_back(data.substr(0, pos));
		data = data.substr(pos + 1);
		pos = data.find(' ');
	}
	strArr.push_back(data);
	vector< vector<string> > keyVal(strArr.size());
	for(auto x: strArr)
	{
		pos = x.find(':');
	}
}
