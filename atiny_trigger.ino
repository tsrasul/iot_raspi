//raspi does not have an ADC, offloading this tom attiny85
//attiny85 does not have a hardware serial to communicate with raspi
#include <SoftwareSerial.h>

//see pins_arduino.c from arduino-tiny core for pin mappings

const int micPin = 2; // output from the amp circuit at AIN2
const int relayPin = 1; // light master relay pin
const int tempPin = 3;  // lm35 temp sensor at AIN3

int maxVal = 0, minVal = 1023;
unsigned long long prevMill = 0;
bool outState = false;
SoftwareSerial mySerial(0, 2); //attiny pin 0 and 2 using softwareserial
#define LEVEL_THRESH 100

void setup()
{
    mySerial.begin(9600);
    pinMode(relayPin, OUTPUT);
}

void loop()
{
    if (mySerial.available())
    {
        // obey any command from the raspberry pi
        char c = mySerial.read();
        if (c == 'P')
        {
            lightOn();
        }
        else if (c == 'N')
        {
            lightOff();
        }
        else if(c == 'T')
        {
            mySerial.println(analogRead(tempPin));
        }
        else
        {
            //did not receive properly, send again?
            mySerial.println("NACK");
        }
    }

    if (calcDiff() > LEVEL_THRESH)
    {
        //detected one peak
        delay(100); // wait 0.1s so the previous sound does not trigger another peak
        prevMill = millis();
        while ((millis() - prevMill) < 1000)
        {
            //wait for one second for the next peak
            if (calcDiff() > LEVEL_THRESH)
            {
                //detected another peak in one second, flip the state
                if (outState)
                {
                    lightOff();
                    //wait 0.1s so the previous sound does not trigger another peak
                    break;
                }
                else
                {
                    lightOn();
                    //wait 0.1s so the previous sound does not trigger another peak
                    break;
                }
            }
        }
    }
}

void lightOn()
{
    outState = true;
    digitalWrite(relayPin, HIGH);
    mySerial.println("high");
    delay(100);
}

void lightOff()
{
    outState = false;
    digitalWrite(relayPin, LOW);
    mySerial.println("low");
    delay(100);
}

int calcDiff()
{
    //this function takes 100 consecutive values from amp and calculates diff between max and min values
    //8MHz is just enough to make it less error prone
    maxVal = 0;
    minVal = 1023;

    for (int iii = 0; iii < 100; iii++)
    {
        int senseData = analogRead(micPin);
        if (senseData > maxVal)
        {
            maxVal = senseData;
        }

        if (senseData < minVal)
        {
            minVal = senseData;
        }
    }
    mySerial.println(maxVal - minVal);
    return (maxVal - minVal);
}